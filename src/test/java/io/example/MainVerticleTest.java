package io.example;

import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;

/**
 * MainVerticle Tester.
 *
 * @version 1.0
 */
@RunWith(VertxUnitRunner.class)
public class MainVerticleTest {

    private Vertx vertx;

    @Before
    public void before(TestContext tc) throws Exception {
        vertx = Vertx.vertx();
        vertx.deployVerticle(MainVerticle.class.getName(), tc.asyncAssertSuccess());
    }

    @After
    public void after(TestContext tc) throws Exception {
        vertx.close(tc.asyncAssertSuccess());
    }

    /**
     * Method: start()
     */
    @Test
    public void testStart(TestContext tc) throws Exception {
//        Async async = tc.async();
//        vertx.createHttpClient().getNow(8080, "localhost", "/", response -> {
//            tc.assertEquals(response.statusCode(), 200);
//            response.bodyHandler(body -> {
//                tc.assertTrue(body.length() > 0);
//                tc.assertEquals(body.toString(), "Hello World!");
//                async.complete();
//            });
//        });
    }


} 
