package io.example.common.annotation;

import io.example.common.enums.RequestMapperType;

import java.lang.annotation.*;

/**
 * Created by Administrator on 2018/3/8.
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})//注解作用于类
public @interface RequestMapperAnnotation {

    RequestMapperType type() default RequestMapperType.SINGLE;
}
