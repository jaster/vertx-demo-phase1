package io.example;

import io.example.common.utils.PropertiesUtils;
import io.vertx.core.VertxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Administrator on 2018/3/8.
 * 继承默认Launcher 可以在Vertx生命周期钩子中加入自己的代码
 */
public class Launcher extends io.vertx.core.Launcher {

    private static final Logger logger = LoggerFactory.getLogger(Launcher.class);

    public static void main(String[] args) {

        //Force to use slf4j
        System.setProperty("vertx.logger-delegate-factory-class-name",
                "io.vertx.core.logging.SLF4JLogDelegateFactory");

        (new Launcher()).dispatch(args);
    }

    @Override
    public void beforeStartingVertx(VertxOptions options) {
        //启动前可以修改vertx选项
        PropertiesUtils p = new PropertiesUtils("config/application.properties");
        String appEnv = p.readProperty("app.env");
        if ("dev".equals(appEnv)) { //开发环境设置EventLoop的阻塞时间为较大值，避免进入断点阻塞时间过长而发出警告
            options.setMaxEventLoopExecuteTime(Long.MAX_VALUE); //如果要检查EventLoop是否有较长时间阻塞，请注释此行
        }
        String appMode = p.readProperty("app.mode");
        if ("clustering".equals(appMode)) {
            options.setClustered(true);
        }
    }

}
