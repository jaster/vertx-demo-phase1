package io.example.model;

/**
 * Created by Administrator on 2018/3/9.
 */
public enum ErrorCodes {
    NO_ACTION_SPECIFIED,
    BAD_ACTION,
    DB_ERROR
}
